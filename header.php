<meta http-equiv="content-type" content="text/html; charset=utf-8"> 
<title>FARMASIA </title> 
<link rel="stylesheet" type="text/css" href="style.css">
<!-- jQuery 1.x -->
<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script> 
<!-- Latest Bootstrap compiled and minified JavaScript -->
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<!-- Latest Bootstrap compiled and minified CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!-- Main CSS -->
<link rel="stylesheet" href="css/style.css">
<!-- Mobile first -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
