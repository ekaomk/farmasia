
<div id="mainContainer" class="">



<div class="topHeader">
    <div class="leftContent ">
    <ul>
      <li><span class="glyphicon glyphicon-menu-hamburger"></span></li>
      <li><span id="btn_signup" data-toggle="modal" data-target="#modal_signup" >SIGN UP</span></li>
      <li><span id="btn_signin" data-toggle="modal" data-target="#modal_signin" >SIGN IN</span></li>
    </ul>
  </div>
</div>
	

<div id="homePageEntersite">
	<div class="title">FARMASIA</div>
  <div class="slogan">INVEST IN YOUR OWN ORGANIC FARM</div>
  <br><br><br><br>
  <div class="centerLogo pull-center">
      <div class="title">FARMASIA</div>
      <div class="slogan">INVEST IN YOUR OWN ORGANIC FARM</div>
  </div>

</div>

  
</div>



<div class="modal fade" id="modal_signin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Sign In</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
          <label for="ip_loginEmail">Email address</label>
          <input type="email" class="form-control" id="ip_loginEmail" placeholder="Enter email" value="tester@tester.com">
        </div>
        <div class="form-group">
          <label for="ip_loginPassword">Password</label>
          <input type="password" class="form-control" id="ip_loginPassword" placeholder="Password">
        </div>
      
        <div class="checkbox">
          <label>
            <input type="checkbox"> Remember me . . .
          </label>
        </div>
                          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="btn_signIn" type="button " class="btn btn-success pull-left">Sign In</button>
      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="modal_signup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Sign Up</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
          <div class="col-md-12 none-padding">
            <div class="col-md-6">
          <div class="input-group regis">
            <span class="input-group-addon" ><span class="glyphicon glyphicon-credit-card"></span></span>
            <input type="text" class="form-control"  id="ip_firstname" placeholder="Enter Firstname" >
          </div>
        </div>
        <div class="col-md-6">
          <div class="input-group regis">
              <span class="input-group-addon" ><span class="glyphicon glyphicon-credit-card"></span></span>
              <input type="text" class="form-control" id="ip_lastname" placeholder="Enter Lastname">
          </div>
        </div>
      
          </div>
      
      <div class="col-md-12">
        <div class="input-group regis">
          <span class="input-group-addon" ><span class="glyphicon glyphicon-time"></span></span>
             <input id="dateStart" class="form-control" data-provide="datepicker" placeholder="Select Birthdate" >
        </div>
      </div>
      <div class="col-md-12">
        <div class="input-group regis">
          <span class="input-group-addon" ><span class="glyphicon glyphicon-envelope"></span></span>
           <input type="email" class="form-control" id="ip_email" placeholder="Enter Email">
        </div>
      </div>
      <div class="col-md-12">
          
        <div class="input-group regis">
           <span class="input-group-addon" ><span class="glyphicon glyphicon-cog"></span></span>
            <input type="password" class="form-control" id="ip_password" placeholder="Enter Password">
        </div>
      </div>
      <div class="col-md-12">
        <div class="input-group regis">
          <span class="input-group-addon" ><span class="glyphicon glyphicon-cog"></span></span>
           <input type="password" class="form-control" id="ip_confirmpassword" placeholder="Enter Confirm-Password">
        </div>
      </div>
    
      
        </div>
        
  
    


     
      </div>
      <div class="modal-footer footerz">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="btn_signUp" type="button " class="btn btn-success pull-left">Sign Up</button>
      </div>
    </div>
  </div>
</div>